local Animation = require "animations/animation"
local PlayerAnimation = {}
local SHOT_FRAME_DURATION = 0.01
local WALK_FRAME_DURATION = 0.1

function PlayerAnimation:initialize()
	-- Register shooting up animation
	local imagePath = "assets/hero/hero_shoot"
	local shotUpAnimation = animationManager:getAnimation("Player", "shootUp")
	if not shotUpAnimation then
		local imagesUpPaths = {}
		for i = 1, 10 do
			imagesUpPaths[i] = imagePath .. i .. ".png"
		end
		imagesUpPaths[11] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[12] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[13] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[14] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[15] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[16] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[17] = "assets/hero/hero_shoot10.png"
		imagesUpPaths[18] = "assets/hero/hero_shoot10.png"

		animationManager:insertAnimation("Player", "shootUp", Animation:new(imagesUpPaths, SHOT_FRAME_DURATION))
	end

	-- Register shooting up animation
	local shotDownAnimation = animationManager:getAnimation("Player", "shootDown")
	if not shotDownAnimation then
		local imagesDownPaths = {}

		imagesDownPaths[1] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[2] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[3] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[4] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[5] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[6] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[7] = "assets/hero/hero_shoot10.png"
		imagesDownPaths[8] = "assets/hero/hero_shoot10.png"

		local currentImage = 9
		for i = 10, 1, -1 do
			imagesDownPaths[currentImage] = imagePath .. i .. ".png"
			currentImage = currentImage + 1
		end
		animationManager:insertAnimation("Player", "shootDown", Animation:new(imagesDownPaths, SHOT_FRAME_DURATION))
	end

	local walkImagePath = "assets/hero/hero_walk"
	-- Register walk right animation
	local walkRightAnimation = animationManager:getAnimation("Player", "walkRight")
	if not walkRightAnimation then
		local imagesWalkRightPaths = {}

		for i = 1, 6 do
			imagesWalkRightPaths[i] = walkImagePath .. i .. ".png"			
		end
		animationManager:insertAnimation("Player", "walkRight", Animation:new(imagesWalkRightPaths, WALK_FRAME_DURATION))
	end

	-- Register walk left animation
	local walkLeftAnimation = animationManager:getAnimation("Player", "walkLeft")
	if not walkLeftAnimation then
		local imagesWalkLeftPaths = {}

		currentImage = 1
		for i = 6, 1, -1 do
			imagesWalkLeftPaths[currentImage] = walkImagePath .. i .. ".png"
			currentImage = currentImage + 1
		end
		
		animationManager:insertAnimation("Player", "walkLeft", Animation:new(imagesWalkLeftPaths, WALK_FRAME_DURATION))
	end

end

return PlayerAnimation