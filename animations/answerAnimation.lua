local Animation = require "animations/animation"
local AnswerAnimation = {}
local color = {"blue", "green", "yellow", "pink", "purple"}

function AnswerAnimation:initialize()
	-- Register color animation
	for j = 1, #color do
		local yellowAnimation = animationManager:getAnimation("Answer", color[j])
		if not yellowAnimation then
			imagePath = "assets/answer/answer_" .. color[j]
			images_paths = {}
			for i = 1, 38 do
				images_paths[i] = imagePath .. i .. ".png"
			end
			animationManager:insertAnimation("Answer", color[j], Animation:new(images_paths, 0.08))
		end
	end 
end

return AnswerAnimation