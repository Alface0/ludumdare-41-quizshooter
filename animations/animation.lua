local Animation = {}

function Animation:new(images_paths, duration)
	local animation = {}

	animation.images = {}
	for i=1, #images_paths do
		animation.images[i] = assetsManager:getAsset(images_paths[i])
	end
	animation.duration = duration or 0.2

	function animation:getCurrentFrame(totalTime)
		elapsedTime = totalTime % (self.duration * #self.images)
		currentFrame = math.floor((elapsedTime / (self.duration * #self.images)) * #self.images) + 1
		return self.images[currentFrame]
	end

	return animation
end

return Animation