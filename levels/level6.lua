local Level = require "levels/level"
local Level6 = {}

function Level6:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "What is the national sport in Japan?"
	questions[1].answers = {"Sumo Wrestling", "Karate", "Football"}

	questions[2] = {}
	questions[2].question = "Who is the dictator of Cuba?"
	questions[2].answers = {"Fidel Castro", "António Salazar", "Joseph Stalin"}

	questions[3] = {}
	questions[3].question = "What is the highest mountain in the world?"
	questions[3].answers = {"Mount Everest", "Wildspitze", "Kilimanjaro"}

	questions[4] = {}
	questions[4].question = "Which animal has got 4 stomachs?"
	questions[4].answers = {"Cow", "Pig", "Dog"}

	questions[5] = {}
	questions[5].question = "What is study of earthquakes?"
	questions[5].answers = {"Seismology", "Astronomy", "Oceanography"}

	questions[6] = {}
	questions[6].question = "What is the smallest state in the world?"
	questions[6].answers = {"Vatican", "San Marino", "Liechtenstein"}

	questions[7] = {}
	questions[7].question = "What do the Japanese people call their own country?"
	questions[7].answers = {"Nippon", "Hispaniola", "Suomi"}

	local level6 = Level:new(questions, 6, 600, 40, "green")
	return level6
end

return Level6