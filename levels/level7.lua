local Level = require "levels/level"
local Level7 = {}

function Level7:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "How long is an Olympic swimming pool?"
	questions[1].answers = {"50m", "25m", "40m"}

	questions[2] = {}
	questions[2].question = "What is the most fractured human bone?"
	questions[2].answers = {"Clavicle", "Tibia", "Radius"}
	
	questions[3] = {}
	questions[3].question = "How many players are on each side of the net in beach volleyball?"
	questions[3].answers = {"2", "6", "4"}
	
	questions[4] = {}
	questions[4].question = "Which country is the origin of the cocktail Mojito?"
	questions[4].answers = {"Cuba", "Mexico", "Argentina"}
	
	questions[5] = {}
	questions[5].question = "What is Japanese sake made from?"
	questions[5].answers = {"Rice", "Potatoes", "Wheat"}
	
	questions[6] = {}
	questions[6].question = "What type of creature is a tarantula?"
	questions[6].answers = {"Spider", "Dog", "Crocodile"}

	questions[7] = {}
	questions[7].question = "Who discovered America?"
	questions[7].answers = {"Christopher Columbus", "Pedro Cabral", "Vasco da Gama"}

	local level7 = Level:new(questions, 7, 700, 35, "green")
	return level7
end

return Level7