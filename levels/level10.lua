local Level = require "levels/level"
local Level10 = {}

function Level10:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "In which country is the highest world mountain?"
	questions[1].answers = {"Nepal", "China", "India"}
	
	questions[2] = {}
	questions[2].question = "Name the world's biggest island?"
	questions[2].answers = {"Greenland", "New Guinea", "Madagascar"}
	
	questions[3] = {}
	questions[3].question = "What is sushi traditionally wrapped in?"
	questions[3].answers = {"Seaweed", "Lettuce leaf", "Tortilla"}
	
	questions[4] = {}
	questions[4].question = "What colour is Absynthe?"
	questions[4].answers = {"Green", "Yellow", "Red"}
	
	questions[5] = {}
	questions[5].question = "Which country is called the 'Land of the Raising Sun'?"
	questions[5].answers = {"Japan", "Jamaica", "Portugal"}

	questions[6] = {}
	questions[6].question = "Which is the correct name for a type of apple?"
	questions[6].answers = {"Granny Smith", "Mammy Smith", "Nanny Smith"}

	questions[7] = {}
	questions[7].question = "The volcano Vesuvius is located in which country?"
	questions[7].answers = {"Italy", "Iceland", "Mexico"}

	local level10 = Level:new(questions, 10, 1000, 30, "yellow")
	return level10
end

return Level10
