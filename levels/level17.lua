local Level = require "levels/level"
local Level17 = {}

function Level17:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "In Greek mythology, who opened a jar out of curiosity and let out all the evils of humanity?"
	questions[1].answers = {"Pandora", "Hera", "Aphrodite"}

	questions[2] = {}
	questions[2].question = "In which European city is the famous Arch of Hadrian?"
	questions[2].answers = {"Athens", "Venice", "Paris"}

	questions[3] = {}
	questions[3].question = "Which planet has the strongest gravity in our solar system?"
	questions[3].answers = {"Jupiter", "Earth", "Mercury"}

	questions[4] = {}
	questions[4].question = "Thermodynamics is the study of what?"
	questions[4].answers = {"Heat", "Artificial Intelligence", "Fluids"}

	questions[5] = {}
	questions[5].question = "A Medusa is what type of marine creature?"
	questions[5].answers = {"Jellyfish", "Sea-horse", "Manta ray"}

	questions[6] = {}
	questions[6].question = "Shiitake is what type of foodstuff?"
	questions[6].answers = {"Mushroom", "Seaweed", "Chilli"}
	
	questions[7] = {}
	questions[7].question = "The crab represents which sign of the Zodiac?"
	questions[7].answers = {"Cancer", "Scorpio", "Virgo"}

	local level17 = Level:new(questions, 17, 2800, 20, "purple")
	return level17
end

return Level17
