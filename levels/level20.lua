local Level = require "levels/level"
local Level20 = {}

function Level20:new()
	questions = {}
	questions[1] = {}
	questions[1].question = "Code Red, Michelangelo and Conficker were all famous examples of what?"
	questions[1].answers = {"Computer viruses", "Alarm systems", "Airplanes"}
	
	questions[2] = {}
	questions[2].question = "Insomniac and Dookie were 1990s albums by which band?"
	questions[2].answers = {"Green Day", "The Offspring", "Bloodhound Gang"}

	questions[3] = {}
	questions[3].question = "What did Marcel Duchamp add to Mona Lisa?"
	questions[3].answers = {"Moustache", "Glasses", "Eye brows"}

	questions[4] = {}
	questions[4].question = "The Term “Impressionism” comes from a work by which painter?"
	questions[4].answers = {"Monet", "Degas", "Pissaro"}
	
	questions[5] = {}
	questions[5].question = "'DB' computer abbreviation usually means ?"
	questions[5].answers = {"Database", "Driver Boot", "Data Block"}
	
	questions[6] = {}
	questions[6].question = "What was the final battle of the Napoleonic Wars?"
	questions[6].answers = {"Waterloo", "Roliça", "Leipzig"}
	
	questions[7] = {}
	questions[7].question = "Cavendish is a variety of what fruit?"
	questions[7].answers = {"Banana", "Kiwi", "Pear"}

	local level20 = Level:new(questions, 20, 4000, 20, "purple")
	return level20
end

return Level20
