local Level = require "levels/level"
local Level12 = {}

function Level12:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "Guatanamo Bay is on which Caribbean island?"
	questions[1].answers = {"Cuba", "Barbados", "Antigua"}
		
	questions[2] = {}
	questions[2].question = "Which is the correct title of a 2012 Justin Bieber hit?"
	questions[2].answers = {"Boyfriend", "Girlfriend", "Husband"}

	questions[3] = {}
	questions[3].question = "'OS' computer abbreviation usually means?"
	questions[3].answers = {"Operating System", "Optical Sensor", "Order of Significance"}

	questions[4] = {}
	questions[4].question = "'.MOV' extension refers usually to what kind of file?"
	questions[4].answers = {"Animation/movie", "Image", "MS Office document"}
	
	questions[5] = {}
	questions[5].question = "With which sport is Michael Jordan associated?"
	questions[5].answers = {"Basketball", "Football", "Darts"}

	questions[6] = {}
	questions[6].question = "According to Thomas Edison, 'genius' is one percent inspirantion and ninety-nine percent what?"
	questions[6].answers = {"Perspiration", "Luck", "Relaxation"}

	questions[7] = {}
	questions[7].question = "Which mammal has the longest gestation period?"
	questions[7].answers = {"Elephant", "Camel", "Zebra"}
	
	local level12 = Level:new(questions, 12, 1400, 30, "yellow")
	return level12
end

return Level12
