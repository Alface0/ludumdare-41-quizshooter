local Level = require "levels/level"
local Level14 = {}

function Level14:new()
	questions = {}
	questions[1] = {}
	questions[1].question = "What is the name of the Sultan’s daughter in 'Aladdin'?"
	questions[1].answers = {"Jasmine", "Mulan", "Esmeralda"}
	
	questions[2] = {}
	questions[2].question = "What is the official language of The Netherlands?"
	questions[2].answers = {"Dutch", "English", "West Frisian"}
		
	questions[3] = {}
	questions[3].question = "Which modern-day country was once known as Zipangu?"
	questions[3].answers = {"Japan", "China", "Korea"}

	questions[4] = {}
	questions[4].question = "In maths, an Icosagon has how many sides?"
	questions[4].answers = {"20", "18", "22"}
	
	questions[5] = {}
	questions[5].question = "Pikachu is one of the species of creatures in which series of games?"
	questions[5].answers = {"Pokemon", "Digimon", "Breath of Fire"}

	questions[6] = {}
	questions[6].question = "Beelzebub is another name for who?"
	questions[6].answers = {"Devil", "Angel", "Human"}

	questions[7] = {}
	questions[7].question = "The first Sonic the Hedgehog game was published by which video game company?"
	questions[7].answers = {"Sega", "Nintendo", "Rockstar"}

	local level14 = Level:new(questions, 14, 1800, 25, "pink")
	return level14
end

return Level14
