local Level = require "levels/level"
local Level4 = {}

function Level4:new()
	local questions = {}
	-- The first answer is always the correct one

	questions[1] = {}
	questions[1].question = "How many members belonged to The Beatles?"
	questions[1].answers = {"4", "3", "5"}
	
	questions[2] = {}
	questions[2].question = "What is the official language of Brazil?"
	questions[2].answers = {"Portuguese", "Spanish", "English"}

	questions[3] = {}
	questions[3].question = "What is the world's longest river?"
	questions[3].answers = {"Amazon", "Nile", "Yangtze"}

	questions[4] = {}
	questions[4].question = "Who played Forest Gump?"
	questions[4].answers = {"Tom Hanks", "Brad Pitt", "Tom Cruise"}

	questions[5] = {}
	questions[5].question = "What is the name of the Founder of Ford Motors?"
	questions[5].answers = {"Henry Ford", "Elon Musk", "Masujiro Hashimoto"}

	questions[6] = {}
	questions[6].question = "Which animal does follow a waving cloth?"
	questions[6].answers = {"Bull", "Monkey", "Eagle"}

	questions[7] = {}
	questions[7].question = "What is the country top-level domain of Portugal?"
	questions[7].answers = {".pt", ".es", ".pr"}

	local level4 = Level:new(questions, 4, 400, 45, "blue")
	return level4
end

return Level4