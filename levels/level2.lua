local Level = require "levels/level"
local Level2 = {}

function Level2:new()
	local questions = {}
	-- The first answer is always the correct one
	questions[1] = {}
	questions[1].question = "What is the largest number of five digits?"
	questions[1].answers = {"99999", "90000", "100000"}

	questions[2] = {}
	questions[2].question = "How many legs do the spiders have?"
	questions[2].answers = {"8", "6", "10"}
	
	questions[3] = {}
	questions[3].question = "What is the world's largest ocean?"
	questions[3].answers = {"Pacific", "Atlantic", "Indian"}

	questions[4] = {}
	questions[4].question = "Who did the Mona Lisa paint?"
	questions[4].answers = {"Leonardo Da Vinci", "Michelangelo", "Diego Velázquez"}

	questions[5] = {}
	questions[5].question = "Which planet is nearest the sun?"
	questions[5].answers = {"Mercury", "Earth", "Venus"}

	questions[6] = {}
	questions[6].question = "What kind of animal is the largest living creature on Earth?"
	questions[6].answers = {"Whale", "Elephant", "Giraffe"}

	questions[7] = {}
	questions[7].question = "Which device do we use to look at the stars?"
	questions[7].answers = {"Telescope", "Microscope", "Stethoscope"}

	local level2 = Level:new(questions, 2 ,200, 55, "blue")
	return level2
end

return Level2