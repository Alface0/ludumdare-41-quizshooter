local Level = require "levels/level"
local Level15 = {}

function Level15:new()
	questions = {}
	questions[1] = {}
	questions[1].question = "Mount Parnassus is in which European country?"
	questions[1].answers = {"Greece", "France", "Italy"}
	
	questions[2] = {}
	questions[2].question = "Which German-born physicist was awarded the 1921 Nobel Prize for Physics?"
	questions[2].answers = {"Albert Einstein", "Gustav Hertz", "Werner Heisenberg"}

	questions[3] = {}
	questions[3].question = "Which Greek philosopher was made to commit suicide by drinking poison?"
	questions[3].answers = {"Socrates", "Demokritos", "Aristotle"}

	questions[4] = {}
	questions[4].question = "What is the modern-day name of the Kingdom of Mauretania?"
	questions[4].answers = {"Morocco", "Liechtenstein", "Land of Ndongo"}
	
	questions[5] = {}
	questions[5].question = "In what year did Vanilla Ice release Ice Ice Baby?"
	questions[5].answers = {"1989", "1997", "1993"}
	
	questions[6] = {}
	questions[6].question = "Its Not Right, But Its Okay was a hit for which female singer?"
	questions[6].answers = {"Whitney Houston", "Celine Dion", "Britney Spears"}
	
	questions[7] = {}
	questions[7].question = "Phobos is a moon of which planet?"
	questions[7].answers = {"Mars", "Saturn", "Jupiter"}

	local level15 = Level:new(questions, 15, 2000, 25, "pink")
	return level15
end

return Level15
