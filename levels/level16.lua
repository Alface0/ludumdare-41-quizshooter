local Level = require "levels/level"
local Level16 = {}

function Level16:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "What colour is the mans tie in the famous work 'Le Fils de L'homme' by Magritte?"
	questions[1].answers = {"Red", "Black", "Green"}

	questions[2] = {}
	questions[2].question = "Which Kings of Leon song reached the highest chart position?"
	questions[2].answers = {"Use Somebody", "Radioactive", "Sex on fire"}

	questions[3] = {}
	questions[3].question = "Pablo Picasso belonged to which school of Art?"
	questions[3].answers = {"Cubist", "Mannerist", "Impressionist"}

	questions[4] = {}
	questions[4].question = "If a coffee bean is darker, will its caffeine content be lower or higher?"
	questions[4].answers = {"Lower", "Higher", "No diffence"}

	questions[5] = {}
	questions[5].question = "Portugal has had six Kings with what first name?"
	questions[5].answers = {"John", "Pedro", "Afonso"}

	questions[6] = {}
	questions[6].question = "A group of which fish is called a Family?"
	questions[6].answers = {"Sardine", "Cod", "Tuna"}
	
	questions[7] = {}
	questions[7].question = "How many paintings did Van Gogh sell during his lifetime?"
	questions[7].answers = {"1", "2", "0"}

	local level16 = Level:new(questions, 16, 2400, 20, "pink")
	return level16
end

return Level16
