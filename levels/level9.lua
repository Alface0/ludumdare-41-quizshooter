local Level = require "levels/level"
local Level9 = {}

function Level9:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "Which planet is closest to Earth?"
	questions[1].answers = {"Venus", "Mars", "Mercury"}
	
	questions[2] = {}
	questions[2].question = "What does 3D stand for?"
	questions[2].answers = {"Three-Dimensional", "Drive-Drill-Drive", "Digital Design Data"}

	questions[3] = {}
	questions[3].question = "Which dwarf planet was originally considered the 9th planet of the solar system?"
	questions[3].answers = {"Pluto", "Eris", "Ceres"}

	questions[4] = {}
	questions[4].question = "What is the name of a community of ants?"
	questions[4].answers = {"Colony", "Drift", "Tribe"}

	questions[5] = {}
	questions[5].question = "Which chemical element has the symbol Pt?"
	questions[5].answers = {"Platinum", "Polonium", "Copper"}

	questions[6] = {}
	questions[6].question = "The Retina is located in which part of the human body?"
	questions[6].answers = {"Eye", "Ear", "Nose"}

	questions[7] = {}
	questions[7].question = "What is the european union capital?"
	questions[7].answers = {"Brussels", "Paris", "Lisbon"}

	local level9 = Level:new(questions, 9, 900, 30, "yellow")
	return level9
end

return Level9
