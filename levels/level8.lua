local Level = require "levels/level"
local Level8 = {}

function Level8:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "On which continent can you visit Mozambique?"
	questions[1].answers = {"Africa", "Europe", "Asia"}

	questions[2] = {}
	questions[2].question = "What is the most populated country?"
	questions[2].answers = {"China", "India", "Russia"}

	questions[3] = {}
	questions[3].question = "Who is the king of all animals?"
	questions[3].answers = {"Lion", "Lynx", "Crocodile"}

	questions[4] = {}
	questions[4].question = "What is the Japanese art of paper-folding called?"
	questions[4].answers = {"Origami", "Kintsugi", "Shou-sugi-ban"}

	questions[5] = {}
	questions[5].question = "What colour is a polar bears fur?"
	questions[5].answers = {"White", "Black", "Brown"}
	
	questions[6] = {}
	questions[6].question = "The 1995 film ‘Toy story’ was created by which computer animation company?"
	questions[6].answers = {"Pixar", "Brain's Base", "Asahi Production"}
	
	questions[7] = {}
	questions[7].question = "Which animal can live without food 2 years?"
	questions[7].answers = {"Tarantula", "Human", "Fly"}

	local level8 = Level:new(questions, 8, 800, 35, "green")
	return level8
end

return Level8