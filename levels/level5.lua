local Level = require "levels/level"
local Level5 = {}

function Level5:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "What is the most spoken language in the world?"
	questions[1].answers = {"Chinese", "English", "Portuguese"}

	questions[2] = {}
	questions[2].question = "Give another name for the study of fossils?"
	questions[2].answers = {"Paleontology", "Gerontology", "Physiology"}

	questions[3] = {}
	questions[3].question = "Which sport does Tiger Woods play?"
	questions[3].answers = {"Golf", "Tennis", "Baseball"}

	questions[4] = {}
	questions[4].question = "Who played Neo in The Matrix?"
	questions[4].answers = {"Keanu Reeves", "Christian Bale", "Liam Neeson"}

	questions[5] = {}
	questions[5].question = "When did the World War II end?"
	questions[5].answers = {"1945", "1942", "1950"}

	questions[6] = {}
	questions[6].question = "What kind of weapon is a katana?"
	questions[6].answers = {"Sword", "Gun", "Knife"}

	questions[7] = {}
	questions[7].question = "Which is the largest sand desert on earth?"
	questions[7].answers = {"Sahara", "Kalahari", "Simpson"}

	local level5 = Level:new(questions, 5, 500, 40, "green")
	return level5
end

return Level5