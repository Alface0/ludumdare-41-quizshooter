local Level = require "levels/level"
local Level19 = {}

function Level19:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "What is the capital of the American state Hawaii?"
	questions[1].answers = {"Honolulu", "Hilo", "Kaneohe"}

	questions[2] = {}
	questions[2].question = "How many women did Henry VIII have?"
	questions[2].answers = {"Six", "Two", "Four"}

	questions[3] = {}
	questions[3].question = "Xerxes ruled a great empire around the fifth century BC. Which empire?"
	questions[3].answers = {"Persian", "Roman", "Indian"}

	questions[4] = {}
	questions[4].question = "What is the name of the network of computers from which the Internet has emerged?"
	questions[4].answers = {"Arpanet", "Deep Web", "Mesh Network"}

	questions[5] = {}
	questions[5].question = "What was the name of AC / DC s lead singer who died in 1980?"
	questions[5].answers = {"Bon Scott", "Stevie Young", "Chris Slade"}

	questions[6] = {}
	questions[6].question = "What is the name of the Indian holy river?"
	questions[6].answers = {"Ganges", "Styx", "Yomi"}
	
	questions[7] = {}
	questions[7].question = "Which Italian film director is considered as the father of the spaghetti western?"
	questions[7].answers = {"Sergio Leone", "Antonio Albanese", "Amleto Palermi"}

	local level19 = Level:new(questions, 19, 3600, 20, "purple")
	return level19
end

return Level19
