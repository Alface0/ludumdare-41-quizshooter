local Level = require "levels/level"
local Level3 = {}

function Level3:new()
	local questions = {}
	-- The first answer is always the correct one

	questions[1] = {}
	questions[1].question = "What type of animal is Bambi?"
	questions[1].answers = {"Deer", "Moose", "Gazelle"}

	questions[2] = {}
	questions[2].question = "What is the capital city of Spain?"
	questions[2].answers = {"Madrid", "Barcelona", "Valencia"}

	questions[3] = {}
	questions[3].question = "Which cheese is traditionally used for pizzas?"
	questions[3].answers = {"Mozzarella", "Brie", "Emmental"}

	questions[4] = {}
	questions[4].question = "Who is the only representative of the animal world, able to draw straight lines?"
	questions[4].answers = {"Human", "Cheetah", "Cat"}

	questions[5] = {}
	questions[5].question = "How many syllables are there in healthy?"
	questions[5].answers = {"2", "1", "3"}

	questions[6] = {}
	questions[6].question = "Which country is the birthplace of the Internet?"
	questions[6].answers = {"USA", "Portugal", "Russia"}

	questions[7] = {}
	questions[7].question = "Which plant does the Canadian flag contain?"
	questions[7].answers = {"Maple", "Clover", "Marijuana"}

	local level3 = Level:new(questions, 3, 300, 50, "blue")
	return level3
end

return Level3