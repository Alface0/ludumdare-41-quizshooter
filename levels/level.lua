local Answer = require "objects/answer"
local Rect = require "objects/rect"
local Level = {}
local QUESTION_HEIGHT = 50
local SLIDE_SPEED = 125
local SLIDE_DURATION = 2

function Level:new(questions, number, score, initialTime, answersColor)
	local level = {}
	level.number = number or 0
	level.score = score or 0
	level.initialTime = initialTime or 0
	level.totalTime = level.initialTime
	level.endAnimationElapsedTime = 0

	local randomQuestion = love.math.random(1, #questions)
	level.answers = {}
	for i = 1, #questions[randomQuestion].answers do
		local isCorrect = false
		
		if i == 1 then
			isCorrect = true
		end
		
		level.answers[i] = Answer:new(
			love.math.random(0, width - ANSWER_WIDTH),
			love.math.random(MIN_ANSWER_Y, MAX_ANSWER_Y - ANSWER_HEIGHT),
			questions[randomQuestion].answers[i], isCorrect, true, answersColor
		)

		local collides = false
		for j = 1, i - 1 do
			if Rect:collide(level.answers[i], level.answers[j]) then
				collides = true
			end
		end

		while collides do
			collides = false
			level.answers[i].position.x = love.math.random(0, width - ANSWER_WIDTH)
			level.answers[i].position.y = love.math.random(MIN_ANSWER_Y, MAX_ANSWER_Y - ANSWER_HEIGHT)

			for j = 1, i - 1 do
				if Rect:collide(level.answers[i], level.answers[j]) then
					collides = true
				end
			end
		end
	end

	level.question = Answer:new(0, 0, questions[randomQuestion].question or "Dummy Question", false, false, "", width, QUESTION_HEIGHT)
	
	function level.question:update(dt)
	end

	level.state = "CREATED"

	function level:load()
		self.question:load()

		for i = 1, #self.answers do
			self.answers[i]:load()
		end

		self.state = "RUNNING"
		self.totalTime = self.initialTime
		gameloop:addGameloop(self)
		renderer:addRenderer(self, 2)
	end

	function level:update(dt)
		if not isGameover and self.state == "RUNNING" then
			self.totalTime = self.totalTime - dt
		end

		if self.state == "FINISHING" then
			level.endAnimationElapsedTime = level.endAnimationElapsedTime + dt
			backgroundY = backgroundY + SLIDE_SPEED * dt
			if level.endAnimationElapsedTime >= SLIDE_DURATION then
				self.state = "FINISHED"
				self:release()
			end
		end
	end

	function level:draw()
		love.graphics.setFont(menuFont)
		love.graphics.print("Time: " .. string.format("%.2f", self.totalTime), width - 125, love.graphics.getHeight() - 35)
		love.graphics.print("Level: " .. string.format("%d", self.number), 18, love.graphics.getHeight() - 60)
	end

	function level:endLevel()
		self.state = "FINISHING"
		
		self.question:release()
		for i = 1, #self.answers do
			self.answers[i]:release()
		end

		self.isInitialized = false
		level.endAnimationElapsedTime = 0
	end

	function level:release()
		gameloop:removeGameloop(self)
		renderer:removeRenderer(self, 2)
	end

	return level
end

return Level