local Level = require "levels/level"
local Level11 = {}

function Level11:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "The Mandible is found on which part of the human body?"
	questions[1].answers = {"Skull", "Leg", "Feet"}

	questions[2] = {}
	questions[2].question = "The Blue Planet is the name often given to which planet?"
	questions[2].answers = {"Earth", "Uranus", "Neptune"}

	questions[3] = {}
	questions[3].question = "How many hours sleep Koalas in day?"
	questions[3].answers = {"18h", "10h", "14h"}
	
	questions[4] = {}
	questions[4].question = "Which English football club is nicknamed ‘The Red Devils’?"
	questions[4].answers = {"Manchester United", "Arsenal", "Chelsea"}

	questions[5] = {}
	questions[5].question = "In computing, how many bits are in one byte?"
	questions[5].answers = {"8", "6", "10"}

	questions[6] = {}
	questions[6].question = "In 1997, what was the first mammal to be cloned from an adult cell?"
	questions[6].answers = {"Dolly", "Little Nicky", "Injaz"}
	
	questions[7] = {}
	questions[7].question = "Who was the first mammal going to space?"
	questions[7].answers = {"Albert II", "Laika", "Tsygan"}
	
	local level11 = Level:new(questions, 11, 1200, 30, "yellow")
	return level11
end

return Level11
