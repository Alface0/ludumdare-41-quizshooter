local Answer = require "objects/answer"
local Level = require "levels/level"
local Level1 = {}

function Level1:new()
	local questions = {}
	-- The first answer is always the correct one
	questions[1] = {}
	questions[1].question = "What color do you get when you mix red and white?"
	questions[1].answers = {"Pink", "Orange", "Purple"}
	
	questions[2] = {}
	questions[2].question = "What does the red circle of the flag of Japan represent?"
	questions[2].answers = {"Sun", "Tomato", "Mars"}
	
	questions[3] = {}
	questions[3].question = "How much is 23 + 15?"
	questions[3].answers = {"38", "37", "39"}

	questions[4] = {}
	questions[4].question = "How much is 32 + 13?"
	questions[4].answers = {"45", "43", "47"}
	
	questions[5] = {}
	questions[5].question = "From which rock band is the song Bohemian Rhapsody?"
	questions[5].answers = {"Queen", "Aerosmith", "Journey"}
	
	questions[6] = {}
	questions[6].question = "What is the capital of England?"
	questions[6].answers = {"London", "Manchester", "New York"}
	
	questions[7] = {}
	questions[7].question = "What is the capital of the USA?"
	questions[7].answers = {"Washington, D.C.", "New York", "Los Angeles"}

	local level1 = Level:new(questions, 1, 100, 60, "blue")
	return level1
end

return Level1