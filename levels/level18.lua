local Level = require "levels/level"
local Level18 = {}

function Level18:new()
	questions = {}

	questions[1] = {}
	questions[1].question = "Stinking Bishop is a variety of which fruit?"
	questions[1].answers = {"Pear", "Apple", "Banana"}

	questions[2] = {}
	questions[2].question = "Which country is the origin of the Stella beer?"
	questions[2].answers = {"Belgium", "Germany", "Scotland"}

	questions[3] = {}
	questions[3].question = "Who discoved one of the first antibiotics: penicillin?"
	questions[3].answers = {"Alexander Fleming", "Jonas Salk", "Emil Adolf Behring"}

	questions[4] = {}
	questions[4].question = "Who was the inventor of the steam engine?"
	questions[4].answers = {"James Watt", "Henry Ford", "John Stevens"}

	questions[5] = {}
	questions[5].question = "Who is the father of the atomic bomb?"
	questions[5].answers = {"Robert Oppenheimer", "Joseph von Fraunhofer", "John Dalton"}

	questions[6] = {}
	questions[6].question = "What do dragonflies prefer to eat?"
	questions[6].answers = {"Mosquitoes", "Flies", "Ants"}
	
	questions[7] = {}
	questions[7].question = "On which Italian island is Palermo?"
	questions[7].answers = {"Sicily", "Palmaria", "Barbana"}

	local level18 = Level:new(questions, 18, 3200, 20, "purple")
	return level18
end

return Level18
