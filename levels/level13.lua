local Level = require "levels/level"
local Level13 = {}

function Level13:new()
	questions = {}
	questions[1] = {}
	questions[1].question = "How many squares are there on a chess board?"
	questions[1].answers = {"64", "48", "72"}
	
	questions[2] = {}
	questions[2].question = "In 'The Simpsons', what is the name of the beer served in Springfield?"
	questions[2].answers = {"Duff", "Corona", "Super Bock"}
		
	questions[3] = {}
	questions[3].question = "What is the hardest natural known substance?"
	questions[3].answers = {"Diamond", "Topaz", "Quartz"}

	questions[4] = {}
	questions[4].question = "In which country did Avocados originate?"
	questions[4].answers = {"Mexico", "Brazil", "Spain"}
	
	questions[5] = {}
	questions[5].question = "The tibia is a part of which area on the human body?"
	questions[5].answers = {"Leg", "Back", "Arm"}

	questions[6] = {}
	questions[6].question = "A group of which fish is called a Lap?"
	questions[6].answers = {"Cod", "Tuna", "Sardine"}

	questions[7] = {}
	questions[7].question = "What is the name for the bending of light as it passes from one medium to another?"
	questions[7].answers = {"Refraction", "Diffraction", "Reflection"}

	local level13 = Level:new(questions, 13, 1600, 25, "pink")
	return level13
end

return Level13
