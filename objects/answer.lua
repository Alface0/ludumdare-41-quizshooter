local Entity = require "objects/entity"
local AnswerAnimation = require "animations/answerAnimation"
local Answer = {}

ANSWER_WIDTH = 166
ANSWER_HEIGHT = 70
MIN_ANSWER_Y = 65
MAX_ANSWER_Y = love.graphics.getHeight() / 2
local ANSWER_MAX_X_VELOCITY = 250
local ANSWER_MAX_Y_VELOCITY = 75

local FONT_SIZE = 14
local font = love.graphics.newFont("assets/BriemAkademiStd-Regular.otf", FONT_SIZE)


function Answer:new(x, y, text, isCorrect, isAnimated, animationColor, width, height)
	local answer = Entity:new(x, y, width or ANSWER_WIDTH, height or ANSWER_HEIGHT, "Answer")

	answer.text = text or "answer"
	answer.isCorrect = isCorrect or false
	answer.velocity.x = love.math.random(-ANSWER_MAX_X_VELOCITY, ANSWER_MAX_X_VELOCITY)
	answer.velocity.y = love.math.random(-ANSWER_MAX_Y_VELOCITY, ANSWER_MAX_Y_VELOCITY)
	answer.direction.x = 1
	answer.direction.y = 1
	answer.isAnimated = isAnimated

	if answer.isAnimated then
		AnswerAnimation:initialize()
		answer.animation = animationManager:getAnimation("Answer", animationColor or "blue")
	end
	answer.elapsedTime = 0

	function answer:load()
		renderer:addRenderer(self, 2)
		gameloop:addGameloop(self)
	end

	function answer:side_update(dt)
		self.elapsedTime = self.elapsedTime + dt
		self.position.x = self.position.x + (self.velocity.x * dt) * self.direction.x

		if self.position.x > love.graphics.getWidth() - ANSWER_WIDTH then
			self.direction.x = self.direction.x * -1
			self.position.x = love.graphics.getWidth() - ANSWER_WIDTH - 1
		elseif self.position.x < 0 then
			self.direction.x = self.direction.x * -1
			self.position.x = 1
		end
	end

	function answer:up_down_update(dt)
		self.elapsedTime = self.elapsedTime + dt
		self.position.x = self.position.x + (self.velocity.x * dt) * self.direction.x
		self.position.y = self.position.y + (self.velocity.y * dt) * self.direction.y

		if self.position.x > love.graphics.getWidth() - ANSWER_WIDTH then
			self.direction.x = self.direction.x * -1
			self.position.x = love.graphics.getWidth() - ANSWER_WIDTH - 1
		elseif self.position.x < 0 then
			self.direction.x = self.direction.x * -1
			self.position.x = 1
		end

		if self.position.y > MAX_ANSWER_Y - ANSWER_HEIGHT then
			self.direction.y = self.direction.y * -1
			self.position.y = MAX_ANSWER_Y - ANSWER_HEIGHT - 1
		elseif self.position.y < MIN_ANSWER_Y then
			self.direction.y = self.direction.y * -1
			self.position.y = MIN_ANSWER_Y + 1
		end
	end

	local randomUpdate = love.math.random(0, 1)
	if randomUpdate == 0 then
		answer.update = answer.side_update
	elseif randomUpdate == 1 then
		answer.update = answer.up_down_update
	end

	function answer:draw()
		if not self.isAnimated then
			love.graphics.setColor(0, 0, 0)
			love.graphics.rectangle("fill", self.position.x, self.position.y, self.size.x, self.size.y)

			love.graphics.setColor(255, 255, 255)
			love.graphics.rectangle("line", self.position.x, self.position.y, self.size.x, self.size.y)
		
			love.graphics.setFont(font)
			love.graphics.printf(answer.text, self.position.x, self.position.y + self.size.y / 2 - (FONT_SIZE / 2), self.size.x, 'center')
		else
			love.graphics.draw(self.animation:getCurrentFrame(self.elapsedTime), self.position.x, self.position.y)
		
			love.graphics.setColor(0, 0, 0)
			love.graphics.setFont(font)
			love.graphics.printf(answer.text, self.position.x, self.position.y + self.size.y / 2 - (FONT_SIZE / 2), self.size.x, 'center')
			love.graphics.setColor(255, 255, 255)
		end
	end

	function answer:release()
		renderer:removeRenderer(self, 2)
		gameloop:removeGameloop(self)
	end

	return answer
end

return Answer