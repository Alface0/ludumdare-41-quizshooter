local Rect = require "objects/rect"
local ScoreBoard = {}
local BOARD_WIDTH = 200
local BOARD_HEIGHT = 220
local SCORES_TO_STORE = 5
local HIGHSCORES_PATH = "highscores.txt"
local SCOREBOARD_MARGIN = 10
local HEADER_MARGIN = 30

function ScoreBoard:new(x, y)
	local scoreBoard = Rect:new(x, y, BOARD_WIDTH, BOARD_HEIGHT)
	scoreBoard.scores = {}
	scoreBoard.lastReceivedScore = 0
	scoreBoard.isActive = false

	-- Load scores from high scores file
	if love.filesystem.exists(HIGHSCORES_PATH) then
		lines = love.filesystem.lines(HIGHSCORES_PATH)
		for line in lines do
			scoreBoard.scores[#scoreBoard.scores + 1] = tonumber(line)
		end
	end

	-- Make sure the highscores are sorted
	table.sort(scoreBoard.scores, function(a, b) return a > b end)

	function scoreBoard:updateScoresFile()
		local highScoresText = ""
		for i = 1, SCORES_TO_STORE do
			local currentScore = 0
			if self.scores[i] then
				currentScore = self.scores[i]
			end

			highScoresText = highScoresText .. tostring(currentScore) .. "\n"
		end

		love.filesystem.write(HIGHSCORES_PATH, highScoresText)
	end

	function scoreBoard:addScore(score)
		table.insert(self.scores, math.floor(score))
		self.lastReceivedScore = score

		-- Order the scores in an ascending order
		table.sort(self.scores, function(a, b) return a > b end)
		self:updateScoresFile()
	end

	function scoreBoard:load()
		self:addScore(player.score)
		renderer:addRenderer(self, 2)
		self.isActive = true
	end

	function scoreBoard:draw()
		love.graphics.setFont(menuFont)
		love.graphics.setColor(0, 0, 0)
		love.graphics.rectangle("fill", self.position.x - self.size.x / 2, self.position.y, self.size.x, self.size.y)

		love.graphics.setColor(255, 255, 255)
		love.graphics.rectangle("line", self.position.x - self.size.x / 2, self.position.y, self.size.x, self.size.y)		
		love.graphics.printf("Highscores", self.position.x - self.size.x / 2, self.position.y + SCOREBOARD_MARGIN, self.size.x, 'center')
		love.graphics.rectangle(
			"line",
			self.position.x - self.size.x / 2 + SCOREBOARD_MARGIN,
			self.position.y + SCOREBOARD_MARGIN + HEADER_MARGIN,
		 	self.size.x - SCOREBOARD_MARGIN * 2,
		 	self.size.y - SCOREBOARD_MARGIN * 2 - HEADER_MARGIN
		 )
		
		local scoresToDisplay = SCORES_TO_STORE
		if #self.scores < SCORES_TO_STORE then 
			scoresToDisplay = #self.scores
		end

		for i=1, scoresToDisplay do
			love.graphics.printf(i .. ".", self.position.x - self.size.x / 2 + SCOREBOARD_MARGIN * 3, self.position.y + i * 30 + HEADER_MARGIN - 5, self.size.x, 'left')
			love.graphics.printf(self.scores[i], self.position.x - self.size.x / 2 + SCOREBOARD_MARGIN * 3, self.position.y + i * 30 + HEADER_MARGIN - 5, self.size.x, 'center')
		end
	end

	function scoreBoard:release()
		renderer:removeRenderer(self, 2)
		self.isActive = false
	end

	return scoreBoard
end

return ScoreBoard