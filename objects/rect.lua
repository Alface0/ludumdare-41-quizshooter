local Vec2 = require "tools/vec2"

local Rect = {}

function Rect:new(x, y, width, height)
	local rect = {}

	rect.position = Vec2:new(x, y)
	rect.size = Vec2:new(width, height)

	function rect:containsPosition(x, y)
		return x >= self.position.x and x <= self.position.x + self.size.x and y >= self.position.y and y <= self.position.y + self.size.y
	end

	return rect
end

function Rect:collide(rect, otherRect)
	local containsFirstPosition = rect:containsPosition(otherRect.position.x, otherRect.position.y)
	local containsSecondPosition = rect:containsPosition(otherRect.position.x + otherRect.size.x, otherRect.position.y)
	local containsThirdPosition = rect:containsPosition(otherRect.position.x, otherRect.position.y + otherRect.size.y)
	local containsFourthPosition = rect:containsPosition(otherRect.position.x + otherRect.size.x, otherRect.position.y + otherRect.size.y)
	return containsFirstPosition or containsSecondPosition or containsThirdPosition or containsFourthPosition
end

return Rect