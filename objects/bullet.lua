local Entity = require "objects/entity"
local Bullet = {}

BULLET_WIDTH = 13
local BULLET_HEIGHT = 18
local BULLET_MAX_VELOCITY = 450
local BULLET_MIN_VELOCITY = 20
local RELEASE_TIME = 0.5
local BULLET_PATH = "assets/bullet/shot.png"
local ROTATED_BULLET_PATH = "assets/bullet/rotated_shot.png"
local bulletShot = love.audio.newSource("assets/tiro.ogg", "static")

function Bullet:new(x, y, targetX, targetY)
	local bullet = Entity:new(x, y, BULLET_WIDTH, BULLET_HEIGHT, "Bullet")

	bullet.velocity.x = bullet.position.x - targetX
	bullet.state = "IN_RELEASE"
	bullet.elapsedTime = 0
	bullet.image = assetsManager:getAsset(BULLET_PATH)
	bullet.rotatedImage = assetsManager:getAsset(ROTATED_BULLET_PATH)

	if bullet.velocity.x < 0 then
		bullet.velocity.x = bullet.velocity.x * -1
		bullet.direction.x = 1
	else
		bullet.direction.x = -1	
	end

	bullet.velocity.y = bullet.position.y - targetY
	if bullet.velocity.y > BULLET_MAX_VELOCITY then
		bullet.velocity.y = BULLET_MAX_VELOCITY
	end

	if bullet.velocity.y < BULLET_MIN_VELOCITY then
		bullet.velocity.y = BULLET_MIN_VELOCITY
	end

	bullet.direction.y = -1

	function bullet:load()
		renderer:addRenderer(self)
		gameloop:addGameloop(self)
		bulletShot:play()
	end

	function bullet:update(dt)
		self.elapsedTime = self.elapsedTime + dt
		if self.state == "IN_RELEASE" and self.elapsedTime >= RELEASE_TIME then
			self.state = "RELEASED"
		end

		self.position.x = self.position.x + (self.velocity.x * dt) * self.direction.x
		self.position.y = self.position.y + (self.velocity.y * dt) * self.direction.y

		if self.position.x >= width - BULLET_WIDTH then
			self.direction.x = -1
			self.position.x = width - BULLET_WIDTH - 1
		end

		if self.position.x <= 0 then
			self.direction.x = 1
			self.position.x = 1
		end
	end

	function bullet:draw()
		if self.direction.y == 1 then
			love.graphics.draw(self.image, self.position.x, self.position.y)
		else
			love.graphics.draw(self.rotatedImage, self.position.x, self.position.y)
		end
	end

	return bullet
end

return Bullet