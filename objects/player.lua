local Entity = require "objects/entity"
local Bullet = require "objects/bullet"
local Rect = require "objects/rect"
local PlayerAnimation = require "animations/playerAnimation"

local Player = {}

local PLAYER_WIDTH = 49
local PLAYER_HEIGHT = 132
local PLAYER_SPEED = 300
local SLOWDOWN_RATIO = 9
local FIRE_COOLDOWN = .5
local BULLET_SCREEN_MARGIN = 20  -- So the player do not lose when out of bullets if they bounce on the walls 
local PLAYER_IMAGE_PATH = "assets/hero/hero_shoot1.png" 
BULLETS_BY_LEVEL = 2

local BULLET_PATH = "assets/bullet/shot2.png"

local SPACE_KEY = "space"
local major, minor, revision, codename = love.getVersion()
if major < 1 then
	SPACE_KEY = " "
end


function Player:new(x, y)
	local player = Entity:new(x - PLAYER_WIDTH / 2, y, PLAYER_WIDTH, PLAYER_HEIGHT, "Player")

	player.timeSinceLastShoot = 10
	player.bullets = {}
	player.usedBullets = 0
	player.isAlive = true
	player.score = 0
	player.image = assetsManager:getAsset(PLAYER_IMAGE_PATH)
	player.bulletImage = assetsManager:getAsset(BULLET_PATH)
	player.elapsedTime = 0
	player.state = "IDLE"

	PlayerAnimation:initialize()
	player.shootUpAnimation = animationManager:getAnimation("Player", "shootUp")
	player.shootDownAnimation = animationManager:getAnimation("Player", "shootDown")
	player.walkRightAnimation = animationManager:getAnimation("Player", "walkRight")
	player.walkLeftAnimation = animationManager:getAnimation("Player", "walkLeft")

	function player:update(dt)
		self.timeSinceLastShoot = self.timeSinceLastShoot + dt
		self.elapsedTime = self.elapsedTime + dt

		if self.isAlive and gamestate ~= "RUNNING" then
			self.state = "IDLE"
		end

		if self.isAlive and gamestate == "RUNNING" then
			if self.state == "IDLE" or self.state == "WALK_LEFT" or self.state == "WALK_RIGHT" then
				if love.keyboard.isDown("left") then
					self.direction.x = -1
					self.velocity.x = PLAYER_SPEED
					if self.state ~= "WALK_LEFT" then
						self.elapsedTime = 0.1
					end
					self.state = "WALK_LEFT"
				end

				if love.keyboard.isDown("right") then
					self.direction.x = 1
					self.velocity.x = PLAYER_SPEED
					if self.state ~= "WALK_RIGHT" then
						self.elapsedTime = 0.1
					end
					self.state = "WALK_RIGHT"
				end

				if not love.keyboard.isDown("right") and not love.keyboard.isDown("left") then
					self.state = "IDLE"
				end



				if love.keyboard.isDown(SPACE_KEY) then
					if self.timeSinceLastShoot >= FIRE_COOLDOWN and self.usedBullets < BULLETS_BY_LEVEL then			
						self.elapsedTime = 0
						self.state = "SHOOTING"
					end
				end

			elseif self.state == "SHOOTING" then
				if self.elapsedTime >= self.shootUpAnimation.duration * #self.shootUpAnimation.images then
					-- Shoot
					self.usedBullets = self.usedBullets + 1
					
					self.bullets[self.usedBullets] = Bullet:new(
						self.position.x + PLAYER_WIDTH / 2 - BULLET_WIDTH / 2, self.position.y, 
						self.position.x + PLAYER_WIDTH / 2 - BULLET_WIDTH / 2, self.position.y - 450
					)
					
					self.bullets[self.usedBullets]:load()
					self.state = "RECOIL"
					self.timeSinceLastShoot = 0
					self.elapsedTime = 0
				end
			elseif self.state == "RECOIL" then
				if self.elapsedTime >= self.shootDownAnimation.duration * #self.shootDownAnimation.images then
					self.state = "IDLE"
					self.elapsedTime = 0
				end

			end


			self.position.x = self.position.x + (self.velocity.x * dt) * self.direction.x
			self.position.y = self.position.y + (self.velocity.y * dt) * self.direction.y
			self.velocity.x = self.velocity.x * (1 - (dt * SLOWDOWN_RATIO))

			if self.position.x > width - PLAYER_WIDTH then
				self.position.x = width - PLAYER_WIDTH
			end

			if self.position.x < 0 then
				self.position.x = 0
			end

			for i= 1, #self.bullets do
				if self.bullets[i].state == "RELEASED" and Rect:collide(self, self.bullets[i]) then
					self.bullets[i]:release()
					self.isAlive = false
					gamestate = "GAMEOVER"
					scoreBoard:load()
					break
				end
			end

			-- Set gameover if the player has no more bullets and they are all out of the screen
			if self.usedBullets >= BULLETS_BY_LEVEL then
				allBulletsOutOfScreen = true
				for i = 1, self.usedBullets do
					if self.bullets[i].position.x >= -BULLET_SCREEN_MARGIN and self.bullets[i].position.x <= love.graphics.getWidth() + BULLET_SCREEN_MARGIN
					and self.bullets[i].position.y >= -BULLET_SCREEN_MARGIN and self.bullets[i].position.y <= love.graphics.getHeight() + BULLET_SCREEN_MARGIN then
						allBulletsOutOfScreen = false 
					end
				end

				if allBulletsOutOfScreen then
					gamestate = "GAMEOVER"
					scoreBoard:load()
				end
			end
		end
	end

	function player:draw()
		if player.isAlive then
			if self.state == "IDLE" then
				love.graphics.draw(self.image, self.position.x, self.position.y)
			elseif self.state == "WALK_RIGHT" then
				love.graphics.draw(self.walkRightAnimation:getCurrentFrame(self.elapsedTime), self.position.x, self.position.y)
			elseif self.state == "WALK_LEFT" then
				love.graphics.draw(self.walkLeftAnimation:getCurrentFrame(self.elapsedTime), self.position.x, self.position.y)
			elseif self.state == "SHOOTING" then
				love.graphics.draw(self.shootUpAnimation:getCurrentFrame(self.elapsedTime), self.position.x, self.position.y)	
			elseif self.state == "RECOIL" then
				love.graphics.draw(self.shootDownAnimation:getCurrentFrame(self.elapsedTime), self.position.x, self.position.y)	
			end
			
			for i=1, BULLETS_BY_LEVEL - self.usedBullets do
				love.graphics.draw(player.bulletImage, width / 2 - 58 + i * 30, love.graphics.getHeight() - 40)
			end
		end
	end

	function player:reloadBullets()
		for i=1, #self.bullets do
			self.bullets[i]:release()
			self.bullets[i] = nil
		end

		player.bullets = {}
		player.usedBullets = 0
	end

	return player
end

return Player