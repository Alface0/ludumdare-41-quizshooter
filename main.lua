local Renderer = require "tools/renderer"
local Gameloop = require "tools/gameloop"
local AssetsManager = require "tools/assetsManager"
local AnimationManager = require "tools/animationManager"
local Player = require "objects/player"
local LevelManager = require "tools/levelManager"
local ScoreBoard = require "objects/scoreBoard"

local ambientMusic = love.audio.newSource("assets/ambient_sound.mp3", "stream")
ambientMusic:setVolume(0.2)
ambientMusic:isLooping(true)

renderer = Renderer:create()
gameloop = Gameloop:create()
assetsManager = AssetsManager:create()
animationManager = AnimationManager:create()

width = love.graphics.getWidth()

elapsedTime = 0
player = Player:new(width / 2, 420)
levelManager = LevelManager:new()
scoreBoard = ScoreBoard:new(width / 2, 225)

local BACKGROUND_PATH = "assets/background.png"
background = assetsManager:getAsset(BACKGROUND_PATH)
BACKGROUND_INITIAL_Y = -5000
backgroundY = BACKGROUND_INITIAL_Y

local FLOOR_PATH = "assets/floor.png"
local floor = assetsManager:getAsset(FLOOR_PATH)
local FLOOR_Y = love.graphics.getHeight() - 147

-- Data to make the restart label blink
local blinkVisibleTime = 0.9
local blinkInvisibileTime = 0.4
local isRestartVisible = true
local restartElapsedTime = 0

FONT_FILE = "assets/BriemAkademiStd-Regular.otf"
local restartFont = love.graphics.newFont(FONT_FILE, 14)
menuFont = love.graphics.newFont(FONT_FILE, 18)
local gameOverFont = love.graphics.newFont(FONT_FILE, 30)
gamestate = "STARTING"

function love.load()
	player:load()
	levelManager:load()
	ambientMusic:play()
	gamestate = "RUNNING"
end


function love.update(dt)
	elapsedTime = elapsedTime + dt
	gameloop:update(dt)
	restartElapsedTime = restartElapsedTime + dt

	if gamestate == "GAMEOVER" or gamestate == "WIN" then
		if love.keyboard.isDown("r") then
			gamestate = "RUNNING"
			player:release()
			player = Player:new(width / 2, 420)
			player:load()

			levelManager:release()
			levelManager = LevelManager:new()
			levelManager:load()

			backgroundY = BACKGROUND_INITIAL_Y
			scoreBoard:release()
		end
	end
end


function love.draw()
	love.graphics.draw(background, 0, backgroundY)
	love.graphics.draw(floor, 0, FLOOR_Y)

	renderer:draw()
	love.graphics.setFont(menuFont)
	love.graphics.print("Score: " .. string.format("%d", player.score), 15, love.graphics.getHeight() - 35)

	if gamestate == "GAMEOVER" or gamestate == "WIN" then
		if gamestate == "GAMEOVER" then
			love.graphics.setFont(gameOverFont)
			love.graphics.print("Game Over", width / 2 - 85, love.graphics.getHeight() / 2 - 150)
		elseif gamestate == "WIN" then
			love.graphics.setFont(gameOverFont)
			love.graphics.print("You Won!", width / 2 - 70, love.graphics.getHeight() / 2 - 150)
		end

		if isRestartVisible then
			love.graphics.setFont(restartFont)
			love.graphics.print("Press R to restart", width / 2 - 63, love.graphics.getHeight() / 2 - 115)
			if restartElapsedTime > blinkVisibleTime then
				isRestartVisible = false
				restartElapsedTime = 0
			end
		else
			if restartElapsedTime > blinkInvisibileTime then
				isRestartVisible = true
				restartElapsedTime = 0
			end
		end
	end
end