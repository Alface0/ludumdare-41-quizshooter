local AssetsManager = {}

function AssetsManager:create()
	local assetsManager = {}
	assetsManager.assets = {}

	function assetsManager:getAsset(name)
		local asset = assetsManager.assets[name]
		if asset then
			return asset
		else
			asset = love.graphics.newImage(name)
			assetsManager.assets[name] = asset
		end

		return asset
	end

	return assetsManager
end

return AssetsManager