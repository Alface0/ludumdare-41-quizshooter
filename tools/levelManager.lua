local Level1 = require "levels/level1"
local Level2 = require "levels/level2"
local Level3 = require "levels/level3"
local Level4 = require "levels/level4"
local Level5 = require "levels/level5"
local Level6 = require "levels/level6"
local Level7 = require "levels/level7"
local Level8 = require "levels/level8"
local Level9 = require "levels/level9"
local Level10 = require "levels/level10"
local Level11 = require "levels/level11"
local Level12 = require "levels/level12"
local Level13 = require "levels/level13"
local Level14 = require "levels/level14"
local Level15 = require "levels/level15"
local Level16 = require "levels/level16"
local Level17 = require "levels/level17"
local Level18 = require "levels/level18"
local Level19 = require "levels/level19"
local Level20 = require "levels/level20"
local LevelManager = {}

local rightAnswerSound = love.audio.newSource("assets/hit_right_answer.ogg", "static")
rightAnswerSound:setVolume(0.3)
local wrongAnswerSound = love.audio.newSource("assets/hit_wrong_answer.ogg", "static")

function LevelManager:new()
	local levelManager = {}
	
	levelManager.levels = {}
	levelManager.levels[1] = Level1:new()
	levelManager.levels[2] = Level2:new()
	levelManager.levels[3] = Level3:new()
	levelManager.levels[4] = Level4:new()
	levelManager.levels[5] = Level5:new()
	levelManager.levels[6] = Level6:new()
	levelManager.levels[7] = Level7:new()
	levelManager.levels[8] = Level8:new()
	levelManager.levels[9] = Level9:new()
	levelManager.levels[10] = Level10:new()
	levelManager.levels[11] = Level11:new()
	levelManager.levels[12] = Level12:new()
	levelManager.levels[13] = Level13:new()
	levelManager.levels[14] = Level14:new()
	levelManager.levels[15] = Level15:new()
	levelManager.levels[16] = Level16:new()
	levelManager.levels[17] = Level17:new()
	levelManager.levels[18] = Level18:new()
	levelManager.levels[19] = Level19:new()
	levelManager.levels[20] = Level20:new()

	levelManager.currentLevel = 0

	function levelManager:load()
		if #self.levels > 0 then
			self.currentLevel = 1
		end

		gameloop:addGameloop(self)
	end

	function levelManager:manageCollisions()
		local level = self.levels[self.currentLevel]

		-- Check collision of the answers with the player bullets
		for i = 1, #level.answers do
			for j = 1, player.usedBullets do
				-- Check if the answer contains the corner points of the bullets to detect if a collision exists
				local containsFirstPosition = level.answers[i]:containsPosition(player.bullets[j].position.x, player.bullets[j].position.y)
				local containsSecondPosition = level.answers[i]:containsPosition(player.bullets[j].position.x + player.bullets[j].size.x, player.bullets[j].position.y)
				local containsThirdPosition = level.answers[i]:containsPosition(player.bullets[j].position.x, player.bullets[j].position.y + player.bullets[j].size.y)
				local containsFourthPosition = level.answers[i]:containsPosition(player.bullets[j].position.x + 
					player.bullets[j].size.x, player.bullets[j].position.y + player.bullets[j].size.y)

				if containsFirstPosition or containsSecondPosition or containsThirdPosition or containsFourthPosition then
					-- Check first if the correct answer was hit, otherwise bounce the bullet
					if level.answers[i].isCorrect then
						-- Base level score * level remaining time + extra 25% by each remaining bullet
						player.score = player.score + level.score * (level.totalTime / level.initialTime) + level.score * (BULLETS_BY_LEVEL - player.usedBullets) / 4
						level:endLevel()
						player:reloadBullets()
						rightAnswerSound:play()
						return ""
					else
						if containsFirstPosition and containsSecondPosition then
							-- Top side collision
							player.bullets[j].direction.y = player.bullets[j].direction.y * -1
							player.bullets[j].position.y = level.answers[i].position.y + level.answers[i].size.y + 2
						elseif containsFirstPosition and containsThirdPosition then
							-- Left side collision
							player.bullets[j].direction.x = player.bullets[j].direction.x * -1
							player.bullets[j].position.x = level.answers[i].position.x + level.answers[i].size.x + 2
						elseif containsThirdPosition and containsFourthPosition then
							-- Bottom side collision
							player.bullets[j].direction.y = player.bullets[j].direction.y * -1
							player.bullets[j].position.y = level.answers[i].position.y - player.bullets[j].size.y - 2
						elseif containsFourthPosition and containsSecondPosition then
							-- Right side collision
							player.bullets[j].direction.x = player.bullets[j].direction.x * -1
							player.bullets[j].position.x = level.answers[i].position.x - player.bullets[j].size.x - 2
						end
						wrongAnswerSound:play()
					end	
				end
			end
		end
	end

	function levelManager:update(dt)
		local level = self.levels[self.currentLevel]
		if level then
			if level.state == "CREATED" then
				level:load()
			elseif level.state == "RUNNING" then
				self:manageCollisions()
				
				if level.totalTime <= 0 then
					level.totalTime = 0
					gamestate = "GAMEOVER"
					if not scoreBoard.isActive then
						scoreBoard:load()
					end
				end
			elseif level.state == "FINISHED" then
				if self.currentLevel < #self.levels then
					self.currentLevel = self.currentLevel + 1
				else
					gamestate = "WIN"
					if not scoreBoard.isActive then
						scoreBoard:load()
					end
				end
			end
		end
	end

	function levelManager:release()
		gameloop:removeGameloop(self)
		local level = self.levels[self.currentLevel]
		level:endLevel()
		level:release()
	end

	return levelManager
end

return LevelManager