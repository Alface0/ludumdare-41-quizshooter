local AnimationManager = {}

function AnimationManager:create()
	local animationManager = {}
	animationManager.animations = {}

	function animationManager:insertAnimation(entityId, animationName, animation)
		if not self.animations[entityId] then
			self.animations[entityId] = {}
		end

		self.animations[entityId][animationName or "default"] = animation
	end

	function animationManager:getAnimation(entityId, animationName)
		local entity_animations = self.animations[entityId]
		if entity_animations == nil then
			return nil
		end

		return entity_animations[animationName or "default"]
	end

	return animationManager
end

return AnimationManager